package com.example.client.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/client-service")
public class ClientController {
    @Value("${spring.application.name}")
	String name;
    
	@GetMapping("/health-check")
	public String status(HttpServletRequest request) {
		return String.format("%s Connected Success, port=%s",
							name, 
							request.getServerPort()); 
	}
}
